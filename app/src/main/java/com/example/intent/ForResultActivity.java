package com.example.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ForResultActivity extends AppCompatActivity {

    private EditText etEmail;
    private Button btnSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_result);

        etEmail = findViewById(R.id.etEmail);
        btnSelect = findViewById(R.id.btnSelect);


        //Event;
        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void finish() {
        //get Text from EditText;
        String userInputEmail = etEmail.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("email",userInputEmail);
        setResult(RESULT_OK, intent);
        super.finish();
    }
}
