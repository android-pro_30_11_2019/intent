package com.example.intent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Request code;
    private static final int REQUEST_CODE = 123; //constant

    //Declare view object;
    private TextView tvUserName;
    private Button btnGoogle;
    private Button btnForResult;
    private TextView tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Bind view object;
        btnGoogle = findViewById(R.id.btnGoogle);
        tvUserName = findViewById(R.id.tvUserName);
        tvUserName.setText("Hello " + getBundleResourceString("username") +" !");
        btnForResult = findViewById(R.id.btnForResult);
        tvEmail = findViewById(R.id.tvEmail);

        //Declare + assign reference;
        btnForResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Code of btnForResult here;
                Intent intent = new Intent(MainActivity.this,ForResultActivity.class);
                startActivityForResult(intent,REQUEST_CODE);
            }
        });



        Button btnIntent = findViewById(R.id.btnIntent);
        btnIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Event Click coding here;
                Toast.makeText(MainActivity.this, "event fired", Toast.LENGTH_SHORT).show();
                //Intent code start: Explicit Intent
                Intent i = new Intent(MainActivity.this,LoginActivity.class);
                //Attach data to Login Acvity;
                i.putExtra("fullName","John Doe");
                i.putExtra("Age",32);
                i.putExtra("gender","M");
                i.putExtra("pwd",123456);

                //Launch Login Activity;
                startActivity(i);
            }
        });
        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse("http://google.com/"));
//                if (i.resolveActivity(getPackageManager()) != null) {
//                    startActivity(i);
//                }




//                Intent intent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("market://details?id=" + "com.khmercreator.khmermusicbox"));
//                if (intent.resolveActivity(getPackageManager()) != null) {
//                    startActivity(intent);
//                }



//                Intent intent = new Intent();
//                intent.setAction(Intent.ACTION_VIEW);
//                String data = String.format("geo:%s,%s", "13.354584", "103.854384");
//                data = String.format("%s?z=%s", data, "100");
//                intent.setData(Uri.parse(data));
//                if (intent.resolveActivity(getPackageManager()) != null) {
//                    startActivity(intent);
//                }
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);

            }
        });

    }
    private String getBundleResourceString(String key){
        String strReturn = "N/A";
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getString(key,strReturn);
        }
        return strReturn;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == this.REQUEST_CODE){
            if(resultCode == RESULT_OK){
                if(data != null){
                    String email = data.getStringExtra("email");
                    tvEmail.setText(email);
                }
            }
        }
    }
}
