package com.example.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    //Log TAG
    private static final String TAG = "INTENT_ACTIVITY";
    //View
    private Button btnLogin;
    private EditText etUserName;
    private EditText etPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //getIntentData();
        //bind View;
        btnLogin = findViewById(R.id.btnLogin);
        etUserName = findViewById(R.id.etUserName);
        etPassword = findViewById(R.id.etPassword);

        //Bind Event;
        btnLogin.setOnClickListener(this);
    }

    private void getIntentData() {
        Toast.makeText(this, "Login Activity is launched", Toast.LENGTH_SHORT).show();
        //Getting data which pass from previous activity via Intent;
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String name = bundle.getString("fullName","N/A");
            int age = bundle.getInt("Age",0);
            String gender = bundle.getString("gender","N/A");
            int pwd = bundle.getInt("pwd",0);
            //print in Console Log;
            Log.e(TAG,"name = "+ name);

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnLogin:
                String username = etUserName.getText().toString();
                String password = etPassword.getText().toString();

                Intent i = new Intent(this,MainActivity.class);
                i.putExtra("username",username);
                i.putExtra("password",password);
                startActivity(i);
                finish();

                break;
        }
    }
}
